
const electron = require('electron');
const url = require('url');
const path = require('path');
const fs = require('fs');
const config = require('electron-json-config');

const { app, BrowserWindow, Menu, ipcMain } = electron;

config.set('dbhost', 'localhost');
config.set('dbuser', 'root');
config.set('dbpass', '4621008811');
config.set('dbname', 'checkin_db');

const uploadsDir = process.platform === 'linux' 
  ? '/home/sanyath007/Desktop/ElectronProjects/electron-demo01/'
  : 'C:\\xampp\\htdocs\\restapi\\public\\uploads\\';

config.set('uploads', uploadsDir);


let mainWindow;
let addItemWindow;
let configWindow;

// Listen for app to be ready
app.on('ready', function() {
  // Create new window
  mainWindow = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true
    }
  });

  //Load html in window
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }));

  // Quit app with main window closed
  mainWindow.on('closed', function() {
    app.quit();
  });

  // Build menu from template
  const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
  
  // Insert menu
  Menu.setApplicationMenu(mainMenu);
});

// Handle createAddItemWindow
function createAddItemWindow() {
  // Create new window
  addItemWindow = new BrowserWindow({
    width: 300,
    height: 200,
    title: 'Add Item'
  });

  addItemWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'addItem.html'),
    protocol: 'file:',
    slashes: true
  }));

  // Handle garbage collection
  addItemWindow.on('closed', function() {
    addItemWindow = null;
  });
}

function createConfigWindow() {
  configWindow = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true
    }
  });

  configWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'config.html'),
    protocol: 'file:',
    slashes: true
  }));

  configWindow.on('closed', function() {
    configWindow = null;
  });
}

// Catch item:add
ipcMain.on('item:add', function(e, item) {
  mainWindow.webContents.send('item:add', item);
  addItemWindow.close();
});

// Create menu template
const mainMenuTemplate = [
  {
    label: 'File',
    submenu: [
      {
        label: 'Add Item',
        click() {
          createAddItemWindow();
        }
      },
      {
        label: 'Clear Item'
      },
      {
        label: 'Config',
        click() {
          createConfigWindow();
        }
      },
      {
        label: 'Quit',
        accelerator: process.platform === 'darwin' ? 'Command+Q' : 'Ctrl+Q', // Key hots
        click() {
          app.quit()
        }
      }
    ]
  }
];

// If mac, add empty object to menu
if(process.platform === 'darwin') {
  mainMenuTemplate.unshift({});
}

// Add developer tools item if not in production
if(process.env.NODE_ENV !== 'production') {
  mainMenuTemplate.push({
    label: 'Developer Tools',
    submenu: [
      {
        label: 'Toggle DevTools',
        accelerator: process.platform === 'darwin' ? 'Command+I' : 'Ctrl+I',
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        }
      },
      {
        role: 'reload'
      }
    ]
  });
}